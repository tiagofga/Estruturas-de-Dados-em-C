# Estruturas-de-Dados-em-C

Adu Ja’Far Mohammed Ibn Musa Alkhowarizmi (780-850), astrônomo e matemático árebe da cidade de Khowarizmi. Escreveu livros de matemática, astronomia e geografia. A álgebra foi introduzida na Europa ocidental por meio de seus trabalhos. A palavra algoritmo surgiu de uma adaptação latina que autores europeus fizeram com o seu nome.

Edsger Wybe Dijkstra foi um cientista da computação holandês conhecido por suas contribuições nas mais diversas areas de desenvolvimento de algoritmos. Recebeu o Prêmio Turing em 1972. Segundo Dijkstra, um algoritmo corresponde a uma descrição de um padrão de comportamento, expresso
em termos de um conjunto finito de ações.

Estrutura de dados e algoritmos estão intimamente relacionados:
- Não se estuda estrutura de dados sem considerar os algoritmos que estão relacionados a cada estrutura.

Dica: Ao escolher um algoritmo para resolver um determinado problema, observe
se a estrutura de dados associada ao mesmo é adequada para solucionar o
problema com eficiência.

Cada estrutura de dados contém um conjunto específico de operações. Logo, a
escolha da melhor estrutura está diretamente relacionada ao tipo de operação
que você está pretendendo executar.
 - Podemos resumir a arte de programar em: Escolha de uma estrutura de
dados eficiente e na construção do algoritmo para executar as operações
relacionadas a essa estrutura.



Este repositório tem como objetivo divulgar o conteúdo de Estruturas de Dados em C.

Para navegar nos assunto acesse os códigos:

- [Tipo Abstrato de Dados (TAD)](https://github.com/tiagofga/Estruturas-de-Dados-em-C/tree/master/TAD).
- [Listas](https://github.com/tiagofga/Estruturas-de-Dados-em-C/tree/master/Lista).
- [Filas](https://github.com/tiagofga/Estruturas-de-Dados-em-C/tree/master/Fila).
- [Pilhas](https://github.com/tiagofga/Estruturas-de-Dados-em-C/tree/master/Pilha).
- [Métodos de Ordenação](https://github.com/tiagofga/Estruturas-de-Dados-em-C/tree/master/MétodosdeOrdenação)
- [Métodos de Pesquisa](https://github.com/tiagofga/Estruturas-de-Dados-em-C/tree/master/MétodosdePesquisa)
- [Árvores](https://github.com/tiagofga/Estruturas-de-Dados-em-C/tree/master/Árvore).
- [Grafo](https://github.com/tiagofga/Estruturas-de-Dados-em-C/tree/master/Grafo).
